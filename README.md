

|      参数      |       说明       |      示例      |
| :------------: | :--------------: | :------------: |
|   customList   |    自定义日期    | ["2021-12-01"] |
|    Multiple    |     是否多选     |     false      |
| historicalDate | 过往日期是否可选 |      true      |

|  事件  |                回调参数                |
| :----: | :------------------------------------: |
| result | {year:"2021",month: "06",day: "11"...} |

![Image text](https://gitee.com/jiang_xiao_lei/calendar_vue/raw/master/src/assets/image-20210622142305069.png)

![Image text](https://gitee.com/jiang_xiao_lei/calendar_vue/raw/master/src/assets/image-20210622142315601.png)
